import 'package:adminevexia/db/sharedPreference.dart';
import 'package:adminevexia/pages/homeSelect.dart';
import 'package:flutter/material.dart';

class Splashscreen extends StatefulWidget {
  @override
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {

  @override
  void initState() {
    super.initState();
    dataCheck();
  }

  Future<void>dataCheck() async{
    String uid = await SharedPrefer.getUid();
    if(uid==null){
      Navigator.of(context).pushNamed('/login_Select');
    }
    else
    {
      Navigator.push(context, MaterialPageRoute(
        builder: (context)=>HomeSelect(userType: uid,)
      ));
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
         child: Image.asset('images/e.png')
      ),
    );
  }
}