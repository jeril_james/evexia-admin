import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class AddDoctorImage extends StatefulWidget {
  final String name,department,description;
  AddDoctorImage({
    this.name,
    this.department,
    this.description
  });
  @override
  _AddDoctorImageState createState() => _AddDoctorImageState();
}

class _AddDoctorImageState extends State<AddDoctorImage> {

  File newPropic;
  String userID ;

  Future getImage() async {
    var temImage = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      newPropic = temImage;
    });
  }
  @override
  void initState() {
    super.initState();
    userID = widget.name;
  }
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(16.0),
              bottomRight: Radius.circular(16.0)),
        ),
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Colors.indigo[300],
        title: Text(
          'Please upload image ...',
          style: TextStyle(
            fontSize: 18.0,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ),
      body: Container(
        child: Center(
          child: newPropic ==null?Text('Select an Image',):enableUpload(),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.only(bottom:18.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              
              RaisedButton(
                elevation: 0,
                // materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                onPressed: getImage,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)),
                color: Colors.indigo[300],
                child: Container(
                  margin: EdgeInsets.only(
                      left: 45.0, right: 45.0, top: 12, bottom: 12),
                  child: Text(
                    'Select Image',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 12.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  Widget enableUpload() {
    return Container(
      child: Column(
        children: <Widget>[
          Image.file(
            newPropic,
            height: 300.0,
            width: 300.0,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: RaisedButton(
              color: Color.fromRGBO(204, 51, 51, 1.0),
              child: Text(
                'Upload',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () async {
                Navigator.of(context).pushNamed('/doctors');
                final StorageReference firebaseStorageRef =
                    FirebaseStorage.instance.ref().child('Doctor_profile-pic/$userID');
                final StorageUploadTask task =
                    firebaseStorageRef.putFile(newPropic);
                StorageTaskSnapshot taskSnapshot = await task.onComplete;
                String url = await taskSnapshot.ref.getDownloadURL();
                Firestore.instance.collection('Users')
                .add({
                  'usertype':'Doctor',
                  'name':widget.name,
                  'department':widget.department,
                  'qualification':widget.description,
                  'image':url
                }).catchError((e){
                  print(e);
                });
              },
            ),
          )
        ],
      ),
    );
  }
}