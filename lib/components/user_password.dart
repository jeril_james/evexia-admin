import 'package:adminevexia/db/password_update.dart';
import 'package:flutter/material.dart';

class UserPassword extends StatefulWidget {
  final String userID;
  UserPassword({
    this.userID
  });
  @override
  _UserPasswordState createState() => _UserPasswordState();
}

class _UserPasswordState extends State<UserPassword> {
  
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String password;
  String name;
  String department;
  String description;
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.close,color: Colors.black),
            onPressed: (){    
              Navigator.of(context).pushNamed('/signup');
            },
          ),
        ),
      body: SingleChildScrollView(
      
              child: Column(
            children:[
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 30.0),
                child: Image(
                  image:AssetImage("images/verification.png"),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 12.0),
                child: Text(
                  "Please fill the details",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 11.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Poppins'
                  ),
                ),
              ),
              Container(
                // height: 180.0,
                margin: EdgeInsets.only(top:12.0,right:12.0,left:12.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: Column(
                  children:[
                    Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Container(
                                    height: 50.0,
                                    margin: const EdgeInsets.only(top: 12.0),
                                    padding: const EdgeInsets.only(
                                        left: 12.0, right: 12.0),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1.5, color: Color(0xdddddddd)),
                                      borderRadius: BorderRadius.circular(12.0),
                                      color: Colors.grey[200].withOpacity(0.6),
                                    ),
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return '\'Name Required\'';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        hintText: "Name",
                                        hintStyle:
                                            TextStyle(fontFamily: 'Poppins'),
                                        border: InputBorder.none,
                                      ),
                                      onChanged: (value) {
                                        this.name = value;
                                      },
                                    ),
                                  ),
                          
                                  Row(
                                    children: <Widget>[
                                      
                                      
                                      Container(
                                        height: 50.0,
                                        width: MediaQuery.of(context).size.width * 0.8,
                                        margin: const EdgeInsets.only(top: 12.0),
                                        padding: const EdgeInsets.only(
                                            left: 12.0, right: 12.0),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 1.5,
                                              color: Color(0xdddddddd)),
                                          borderRadius:
                                              BorderRadius.circular(12.0),
                                          color:
                                              Colors.grey[200].withOpacity(0.6),
                                        ),
                                        child: DropdownButtonFormField(
                                          value: department,
                                          hint: Text('Department'),
                                          items: <String>[
                                            'General Medicine',
                                            'Coronavirus treatment',
                                            'Geriatric',
                                            'Lab',
                                            'Blood Bank',
                                            'First Aid',
                                            'Paediatric',
                                            'Urology',
                                          ].map<DropdownMenuItem<String>>(
                                              (String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                          onChanged: (value) {
                                            setState(() {
                                              this.department = value;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    height: 150.0,
                                    margin: const EdgeInsets.only(top: 12.0),
                                    padding: const EdgeInsets.only(
                                        left: 12.0, right: 12.0),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1.5, color: Color(0xdddddddd)),
                                      borderRadius: BorderRadius.circular(12.0),
                                      color: Colors.grey[200].withOpacity(0.6),
                                    ),
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return '\'Name Required\'';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: "Qualification",
                                        hintStyle:
                                            TextStyle(fontFamily: 'Poppins'),
                                      ),
                                      onChanged: (value) {
                                        this.description = value;
                                      },
                                      keyboardType: TextInputType.multiline,
                                     
                                    ),
                                  ),
                                  Container(
                                    height: 50.0,
                                    margin: const EdgeInsets.only(top: 12.0),
                                    padding: const EdgeInsets.only(
                                        left: 12.0, right: 12.0),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1.5, color: Color(0xdddddddd)),
                                      borderRadius: BorderRadius.circular(12.0),
                                      color: Colors.grey[200].withOpacity(0.6),
                                    ),
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return '\'Name Required\'';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        hintText: "Password",
                                        hintStyle:
                                            TextStyle(fontFamily: 'Poppins'),
                                        border: InputBorder.none,
                                      ),
                                      onChanged: (value) {
                                        this.password = value;
                                      },
                                      obscureText: true,
                                    ),
                                  ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top:50.0,bottom: 20.0,right:30.0,left: 30.0 ),
                      height: 40,
                      child: RaisedButton(
                        color: Colors.indigo[300],
                        onPressed: (){
                          loading = true;
                          PasswordUpdate().updatePassword(
                            this.password,
                            this.name,
                            this.department,
                            this.description,
                            widget.userID,
                            context);
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: Container(
                          child:  Center(
                            child: loading==false?Text(
                              'Submit', 
                              style:  TextStyle(
                                fontSize: 18.0, 
                                color: Colors.white,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                              ),
                            ) : CircularProgressIndicator(),  
                          ),
                        ),   
                      ),
                    ),      
                  ],
                ),
              ),
            ],
          ),
      ),
    );
  }
}