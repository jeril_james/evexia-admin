import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefer{
  static Future<void>setUid(String uid)async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('uid', uid);
  }
  static Future<String> getUid() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("uid");
  }
  // static Future<void>setUser(String uid)async{
  //   SharedPreferences preferences = await SharedPreferences.getInstance();
  //   preferences.setString('uid', uid);
  // }
  // static Future<String> getUser() async{
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   return prefs.getString("uid");
  // }
}