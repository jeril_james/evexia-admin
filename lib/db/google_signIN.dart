import 'package:adminevexia/components/user_password.dart';
import 'package:adminevexia/db/sharedPreference.dart';
import 'package:adminevexia/pages/driver/driver_details.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class GoogleUserManagement{
  Future<void> storeNewuser(FirebaseUser user, String userType, context) async{
   final snapshot = await Firestore.instance.collection('/Users').document(user.uid).get();
   if(snapshot == null || !snapshot.exists){
     await Firestore.instance.collection('/Users').document(user.uid).setData(
    ({
      'customerid': user.uid,
      'email': user.email,
      'usertype': userType,
      'verification':'Pending',
    })).then((value) {
      // SharedPrefer.setUid(user.uid);
      Navigator.of(context).pop();
      if(userType=='Doctor')
      {
        SharedPrefer.setUid(user.uid);
        Navigator.push(context, MaterialPageRoute(builder: (context)=>UserPassword(
        userID: user.uid,
      )));
      }else{
        SharedPrefer.setUid(user.uid);
        Navigator.push(context, MaterialPageRoute(builder: (context)=>DriverDetails(userID: user.uid,)));
      }
    }).catchError((e) {
      print(e);
    });
   }
   else{
     Fluttertoast.showToast(msg: 'User Already exist! Please login');
   }
  }
}