import 'package:adminevexia/db/sharedPreference.dart';
import 'package:adminevexia/pages/homeSelect.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginUSerManagement {
  
  Future<void> accessUser(FirebaseUser user,context)async{
    final snapshot = await Firestore.instance.collection('/Users').document(user.uid).get();
    if(snapshot==null || !snapshot.exists){
      await Fluttertoast.showToast(msg: 'No User Exist');
    }
    else{
      await Firestore.instance.collection('/Users').document(user.uid).updateData({
       'customerid': user.uid,
      'email': user.email,
      }).then((value){
        SharedPrefer.setUid(user.uid);
        Navigator.of(context).pop();
       Navigator.push(context, MaterialPageRoute(
         builder: (context)=>HomeSelect(
           userType: user.uid,
         )
       ));
      }).catchError((e){
        print(e);
      });
    }

  }
}