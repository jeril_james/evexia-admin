import 'package:cloud_firestore/cloud_firestore.dart';

class ProfileCrud{
  Future<void>addData(profileDetails,id)async{
    Firestore.instance.collection('Users').document(id).updateData(profileDetails)
    .catchError((e){
      print(e);
    });
  }
}