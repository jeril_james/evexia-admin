import 'package:adminevexia/db/sharedPreference.dart';
import 'package:adminevexia/pages/verificationPending.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PasswordUpdate{
  Future<void> updatePassword(String password, String name, String department, String description ,String userId,context) async{
   await Firestore.instance.collection('/Users').document(userId).updateData(
    ({
      'password': password,
      'name':name,
      'department':department,
      'description':description,
    })).then((value) {
      SharedPrefer.setUid(userId);
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      Navigator.push(context, MaterialPageRoute(builder: (context)=>VerificationPending(
      )));
    }).catchError((e) {
      print(e);
    });
}
}