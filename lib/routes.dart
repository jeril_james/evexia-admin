import 'package:adminevexia/all_enquiries.dart';
import 'package:adminevexia/components/splashscreen.dart';
import 'package:adminevexia/pages/add_doctor.dart';
import 'package:adminevexia/pages/doctor/doctor_chat.dart';
import 'package:adminevexia/pages/doctor/doctor_image.dart';
import 'package:adminevexia/pages/doctor/doctor_myPage.dart';
import 'package:adminevexia/pages/doctor/doctor_profile_edit.dart';
import 'package:adminevexia/pages/doctor/my_cases.dart';
import 'package:adminevexia/pages/doctors.dart';
import 'package:adminevexia/pages/driver/driver_account.dart';
import 'package:adminevexia/pages/driver/driver_cases.dart';
import 'package:adminevexia/pages/driver/driver_image.dart';
import 'package:adminevexia/pages/driver/driver_profile_edit.dart';
import 'package:adminevexia/pages/driver/drivers.dart';
import 'package:adminevexia/pages/homepage.dart';
import 'package:adminevexia/pages/login.dart';
import 'package:adminevexia/pages/login_select.dart';
import 'package:flutter/material.dart';

class RouteGenerator{
  static Route<dynamic> generateRoute(RouteSettings settings){
    final args = settings.arguments;
    switch(settings.name){
      case '/':
        return MaterialPageRoute(builder: (_)=>Splashscreen());
      case '/home':
        return MaterialPageRoute(builder: (_)=>Homepage());
      case '/login_Select':
        return MaterialPageRoute(builder: (_)=>LoginSelect());
      case '/login':
        return MaterialPageRoute(builder: (_)=>LoginPage());
      case '/doctors':
        return MaterialPageRoute(builder: (_)=>Doctors());
      case '/drivers':
        return MaterialPageRoute(builder: (_)=>Drivers());
      case '/all_doctor':
        return MaterialPageRoute(builder: (_)=>AllDoctor());
      case '/my_cases':
        return MaterialPageRoute(builder: (_)=>Mycases());
      case '/doctor_user':
        return MaterialPageRoute(builder: (_)=>DoctorUserPage());
      case '/doctor_profile_edit':
        return MaterialPageRoute(builder: (_)=>DoctorProfileEdit());
      case '/doctor_image':
        return MaterialPageRoute(builder: (_)=>DoctorImage());
      case '/driver_user':
        return MaterialPageRoute(builder: (_)=>DriverrUserPage());
      case '/driver_profile_edit':
        return MaterialPageRoute(builder: (_)=>DriverProfileEdit());
      case '/driver_image':
        return MaterialPageRoute(builder: (_)=>DriverImage());
      case '/driver_cases':
        return MaterialPageRoute(builder: (_)=>DriverCases());
      case '/all_enquiries':
        return MaterialPageRoute(builder: (_)=>AllEnquiries());
      case '/doctor_chat':
        return MaterialPageRoute(builder: (_)=>DoctorChat());
      default: return _errorRoute();
    }
  }
  static Route<dynamic> _errorRoute(){
    return MaterialPageRoute(
      builder: (_){
        return Scaffold(
          appBar: AppBar(
            title: Text('Error'),
          ),
          body:Center(
            child: Text('Error'),
          )
        );
      }
    );
  }
}