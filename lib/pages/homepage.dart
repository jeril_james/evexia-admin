import 'package:adminevexia/db/sharedPreference.dart';
import 'package:adminevexia/pages/enquiryDetails.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(16.0),
              bottomRight: Radius.circular(16.0)),
        ),
        elevation: 0,
        backgroundColor: Colors.indigo[300],
        title: Text(
          'All Enquiries ...',
          style: TextStyle(
            fontSize: 18.0,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            InkWell(
              onTap: () {
                Navigator.of(context).pushNamed('/home');
              },
              child: ListTile(
                title: Text('Home'),
                leading: Icon(
                  Icons.home,
                  color: Colors.indigo[300],
                ),
              ),
            ),
            // InkWell(
            //   onTap: (){
            //     Navigator.of(context).pushNamed('/user_profile');
            //   },
            //               child: ListTile(
            //     title: Text('My Account'),
            //     leading: Icon(Icons.person, color: Colors.indigo[300]),
            //   ),
            // ),
            InkWell(
              onTap: (){
                Navigator.of(context).pushNamed('/doctors');
              },
                          child: ListTile(
                title: Text('Doctors'),
                leading: Icon(Icons.local_hospital, color: Colors.indigo[300]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Divider(),
            ),
            ListTile(
              title: Text('Settings'),
              leading: Icon(Icons.settings, color: Colors.indigo[300]),
            ),
            ListTile(
              title: Text('About us'),
              leading: Icon(Icons.group, color: Colors.indigo[300]),
            ),
            ListTile(
              title: Text('Contact us'),
              leading: Icon(Icons.help, color: Colors.indigo[300]),
            ),
            InkWell(
              onTap: (){
                SharedPrefer.setUid(null);
                  Navigator.of(context).pushNamed('/login_Select');
              },
                          child: ListTile(
                title: Text('Logout'),
                leading: Icon(Icons.exit_to_app, color: Colors.indigo[300]),
              ),
            ),
          ],
        ),
      ),
      body: StreamBuilder(
                stream: Firestore.instance
                    .collection('Enquiries').where('status', isEqualTo: 'Pending')
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    return ListView.builder(
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) {
                        DateTime date = snapshot
                            .data.documents[index]['Enquiry date']
                            .toDate();
                        return InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EnquiryDetails(
                                        documentSnapshot: snapshot.data.documents[index],
                                )));
                          },
                          child: Container(
                            height: 130.0,
                            padding: EdgeInsets.all(26.0),
                            width: double.maxFinite,
                            margin: EdgeInsets.only(
                                left: 14.0, right: 14.0, bottom: 5.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey.withOpacity(0.4),
                                    offset: Offset(2, 4),
                                    blurRadius: 10)
                              ],
                              color: Colors.white,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      snapshot.data.documents[index]['Service'],
                                      style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                    ),
                                    Container(
                                      height: 30.0,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 1.5,
                                              color: Colors.indigo[300]),
                                          borderRadius:
                                              BorderRadius.circular(20.0)),
                                      child: FlatButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20.0)),
                                        onPressed: () {
                                          Navigator.of(context)
                                              .pushNamed('/enquiries');
                                        },
                                        child: Text(
                                          'More Details',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.indigo[300],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: 30.0,
                                    child: Center(
                                      child: Container(
                                        height: 1.0,
                                        color:
                                            Colors.grey[400].withOpacity(0.1),
                                      ),
                                    )),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Date :' +
                                          date.toString().substring(0, 11),
                                      style: TextStyle(
                                        fontFamily: 'Poppins',
                                        fontSize: 10.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.red,
                                      ),
                                    ),
                                    Text(
                                      'Status : ' +
                                          snapshot.data.documents[index]
                                              ['status'],
                                      style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 10.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  }
                },
              ),
    );
  }
}