import 'package:adminevexia/db/google_signIN.dart';
import 'package:adminevexia/db/loginUserManagement.dart';
import 'package:adminevexia/db/sharedPreference.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String username;
  String password;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  String lastvalue;
  Future<String> uid;
  String userType;
  String userTyPe_detect;

  Future<String> signInWithGoogle(String userTyPe) async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleSignInAuthentication.idToken,
        accessToken: googleSignInAuthentication.accessToken);
    final AuthResult authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;
    GoogleUserManagement().storeNewuser(user, this.userType, context);
    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    return 'signInS';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xeeffffff),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.close, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushNamed('/signup');
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 30.0),
              child: Image(
                image: AssetImage("images/verification.png"),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 12.0),
              child: Text(
                "Enter your username and password!",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 11.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Poppins'),
              ),
            ),
            Container(
              height: 350.0,
              margin: EdgeInsets.only(top: 12.0, right: 12.0, left: 12.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12.0),
              ),
              child: Column(
                children: [
                  Form(
                    child: Column(
                      children: <Widget>[
                        // Row(
                        //   children: <Widget>[
                        //     Expanded(
                        //       child: Container(
                        //         height: 40.0,
                        //         margin: EdgeInsets.only(
                        //             top: 20.0, left: 12.0, right: 12.0),
                        //         decoration: BoxDecoration(
                        //           border: Border.all(
                        //               width: 1.0, color: Color(0xdddddddd)),
                        //           borderRadius: BorderRadius.circular(12.0),
                        //         ),
                        //         child: Center(
                        //           child: TextField(
                        //             decoration: InputDecoration(
                        //               contentPadding: EdgeInsets.only(
                        //                   top: 6, left: 12, right: 12),
                        //               border: InputBorder.none,
                        //               suffixIcon: Icon(Icons.verified_user),
                        //               hintText: 'Username',
                        //               prefixStyle:
                        //                   TextStyle(color: Colors.black),
                        //             ),
                        //             onChanged: (value) {
                        //               this.username = value;
                        //             },
                        //           ),
                        //         ),
                        //       ),
                        //     ),
                        //   ],
                        // ),
                        // Row(
                        //   children: <Widget>[
                        //     Expanded(
                        //       child: Container(
                        //         height: 40.0,
                        //         margin: EdgeInsets.only(
                        //             top: 20.0, left: 12.0, right: 12.0),
                        //         decoration: BoxDecoration(
                        //           border: Border.all(
                        //               width: 1.0, color: Color(0xdddddddd)),
                        //           borderRadius: BorderRadius.circular(12.0),
                        //         ),
                        //         child: Center(
                        //           child: TextField(
                        //             decoration: InputDecoration(
                        //               contentPadding: EdgeInsets.only(
                        //                   top: 6, left: 12, right: 12),
                        //               border: InputBorder.none,
                        //               suffixIcon: Icon(Icons.security),
                        //               hintText: 'Password',
                        //               prefixStyle:
                        //                   TextStyle(color: Colors.black),
                        //             ),
                        //             onChanged: (value) {
                        //               this.password = value;
                        //             },
                        //             obscureText: true,
                        //           ),
                        //         ),
                        //       ),
                        //     ),
                        //   ],
                        // ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: 50.0, bottom: 20.0, right: 30.0, left: 30.0),
                    height: 40,
                    child: RaisedButton(
                      color: Colors.indigo[300],
                      onPressed: ()async {
                        final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
                        final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;
                        final AuthCredential credential = GoogleAuthProvider.getCredential(idToken: googleSignInAuthentication.idToken, accessToken: googleSignInAuthentication.accessToken);
                        final AuthResult authResult = await _auth.signInWithCredential(credential);
                        final FirebaseUser user = authResult.user;
                        LoginUSerManagement().accessUser(user, context);
                        assert(!user.isAnonymous);
                        assert(await user.getIdToken()!=null);
                        final FirebaseUser currentUser = await _auth.currentUser();
                        assert(user.uid == currentUser.uid);

                        return 'Signin';
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      child: Container(
                        child: Center(
                          child: Text(
                            'Login',
                            style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.white,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(top: 30.0),
                    child: Text(
                      "Don't have an account?",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Poppins'),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        FlatButton(
                          onPressed: () {
                            showCupertinoDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return CupertinoAlertDialog(
                                    title: Text('Select any'),
                                    actions: <Widget>[
                                      CupertinoDialogAction(
                                        child: Text('Doctor'),
                                        onPressed: () {
                                          setState(() {
                                            userType = 'Doctor';
                                            signInWithGoogle(this.userType);
                                          });
                                        },
                                      ),
                                      CupertinoDialogAction(
                                        child: Text('Driver'),
                                        onPressed: () {
                                          setState(() {
                                            userType = 'Driver';
                                            signInWithGoogle(this.userType);
                                          });
                                        },
                                      )
                                    ],
                                  );
                                });
                          },
                          child: Text(
                            " Create it now!",
                            style: TextStyle(
                                color: Colors.indigo[300],
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Poppins'),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
