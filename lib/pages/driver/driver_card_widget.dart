import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class DriverCardWidget extends StatefulWidget {

  final DocumentSnapshot documentSnapshot;
  DriverCardWidget({
    this.documentSnapshot,
  });


  @override
  _DriverCardWidgetState createState() => _DriverCardWidgetState();
}

class _DriverCardWidgetState extends State<DriverCardWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150.0,
        padding: const EdgeInsets.all(6.0),
        child:FlatButton(        
          highlightColor: Colors.white,
          
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16.0),
          ),
          child: Card(
            elevation:0.2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16.0),
            ),
            child: Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.1), offset: Offset(0,4), blurRadius: 10)
                ],
              ),
              padding: const EdgeInsets.only(top:12.0,bottom: 12.0,left: 12.0,right: 12.0),
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        ball(widget.documentSnapshot['image']??'',Colors.transparent),
                        Text(
                          widget.documentSnapshot.data['name']??'',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 15.0,
                            color: Colors.red,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Container(
                                          height: 30.0,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  width: 1.5,
                                                  color: Colors.indigo[300]),
                                              borderRadius:
                                                  BorderRadius.circular(20.0)),
                                          child: FlatButton(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20.0)),
                                            onPressed: () {
                                             Firestore.instance.collection('/Users').document(widget.documentSnapshot.documentID).updateData({
                                               'verification':'Verified'
                                             });
                                             Navigator.of(context).pushNamed('/drivers');
                                            },
                                            child: Text(
                                              'Accept?',
                                              style: TextStyle(
                                                fontFamily: 'Poppins',
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.indigo[300],
                                              ),
                                            ),
                                          ),
                                        ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            
                            Container(
                              margin: const EdgeInsets.only(bottom: 6.0),
                              child: Text(
                                widget.documentSnapshot.data['department']??'',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Container(
                              width: 200.0,
                              padding: const EdgeInsets.all(6.0),
                              decoration: BoxDecoration(
                                border: Border.all(width: 1,color: Colors.grey.withOpacity(0.1)),
                                borderRadius: BorderRadius.circular(12),
                              ),
                              child:Text(
                                widget.documentSnapshot.data['description']??'',
                                style: TextStyle(
                                  fontFamily: 'Poppins',
                                  color:Colors.grey,
                                  fontSize: 13.0
                                ),
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                               'Verification : ',
                                style: TextStyle(
                                  fontFamily: 'Poppins',
                                  color:Colors.grey,
                                  fontSize: 13.0
                                ),
                              ),
                                Text(
                                widget.documentSnapshot.data['verification']??'',
                                style: TextStyle(
                                  fontFamily: 'Poppins',
                                  color:Colors.grey,
                                  fontSize: 13.0
                                ),
                              ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
    ),
    );
  }
  Widget ball(image,Color color){
    return Container(
      height: 60,width: 60.0,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(100.0),
        image: DecorationImage(image: NetworkImage(image), fit: BoxFit.cover,
      ),

      ),
      
    );
  }
}