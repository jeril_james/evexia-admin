import 'dart:io';

import 'package:adminevexia/db/sharedPreference.dart';
import 'package:adminevexia/pages/homeSelect.dart';
import 'package:adminevexia/pages/mapview.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sliding_button/sliding_button.dart';

class DriverMyCaseDetails extends StatefulWidget {
  final DocumentSnapshot documentSnapshot;
  DriverMyCaseDetails({this.documentSnapshot});
  @override
  _DriverMyCaseDetailsState createState() => _DriverMyCaseDetailsState();
}

class _DriverMyCaseDetailsState extends State<DriverMyCaseDetails> {
  File recording;
  String id;
  @override
  void initState() {
    super.initState();
    _getId();
  }
  void _getId() async {
    id = await SharedPrefer.getUid();
  }

  @override
  Widget build(BuildContext context) {
    DateTime date = widget.documentSnapshot['Enquiry date'].toDate();
    String docID = widget.documentSnapshot.documentID;
    double latitudeMap = widget.documentSnapshot['latitude'];
    double longitudeMap = widget.documentSnapshot['longitude'];
    String name = widget.documentSnapshot['customer_name'];
    // File record = File.fromUri(widget.documentSnapshot['audio']);

    // setState(() {
    //   recording = File(widget.documentSnapshot['audio']);
    // });
    return Scaffold(
        appBar: AppBar(
          // shape: RoundedRectangleBorder(
          //   borderRadius: BorderRadius.only(
          //       bottomLeft: Radius.circular(16.0),
          //       bottomRight: Radius.circular(16.0)),
          // ),
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          backgroundColor: Colors.indigo[300],
          title: Text(
            'Enquiry Details ...',
            style: TextStyle(
              fontSize: 18.0,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: 150,
                    padding:
                        const EdgeInsets.only(top: 40, left: 12.0, right: 12.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(25.0),
                          bottomRight: Radius.circular(25.0)),
                      color: Colors.indigo[300],
                    ),
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(26.0),
                        margin:
                            EdgeInsets.only(top: 33.0, left: 14.0, right: 14.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.4),
                                offset: Offset(2, 4),
                                blurRadius: 10)
                          ],
                          color: Colors.white,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  name ?? 'aaa',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.green,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Enquiry date :  " +
                                      date.toString().substring(0, 11),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 14.0,
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "Address : ",
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 20.0,
                                    color: Colors.indigo[300],
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              widget.documentSnapshot['address'] ?? '',
                              style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 14.0,
                                color: Colors.black,
                              ),
                              textAlign: TextAlign.justify,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(26.0),
                    margin: EdgeInsets.only(top: 15.0, left: 14.0, right: 14.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.4),
                            offset: Offset(2, 4),
                            blurRadius: 10)
                      ],
                      color: Colors.white,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Service :  " +
                                  widget.documentSnapshot['Service'],
                              style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.green,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Status :  " + widget.documentSnapshot['status'],
                              style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 14.0,
                                color: Colors.black,
                              ),
                            ),
                            Text(
                              "Enquiry date :  " +
                                  date.toString().substring(0, 11),
                              style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 14.0,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MapView(
                                lati: latitudeMap,
                                lang: longitudeMap,
                              )));
                },
                child: Stack(
                  children: <Widget>[
                    Container(
                      // padding:EdgeInsets.all(26.0),
                      margin:
                          EdgeInsets.only(top: 15.0, left: 14.0, right: 14.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.4),
                              offset: Offset(2, 4),
                              blurRadius: 10)
                        ],
                        color: Colors.white,
                      ),
                      child: Container(
                        height: 300.0,
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: GoogleMap(
                          onTap: (latlang) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MapView(
                                          lati: latitudeMap,
                                          lang: longitudeMap,
                                        )));
                          },
                          mapType: MapType.normal,
                          initialCameraPosition: CameraPosition(
                              target: LatLng(latitudeMap, longitudeMap),
                              zoom: 15.0),
                          scrollGesturesEnabled: true,
                          markers: Set<Marker>.of(<Marker>[
                            Marker(
                              draggable: true,
                              markerId: MarkerId('1'),
                              position: LatLng(latitudeMap, longitudeMap),
                            )
                          ]),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: SlidingButton(
          // key: _slideButtonkEY,
          buttonColor: Colors.indigo[300],
          buttonText: 'Slide to Confirm and Close Enquiry...',
          slideButtonIconColor: Colors.indigo[300],
          onSlideSuccessCallback: () {
            Firestore.instance
                .collection('Enquiries')
                .document(widget.documentSnapshot.documentID)
                .updateData({
                  'driver-status':'case closed'
                }).then((result) {
                 Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => HomeSelect(
                            userType: id,
                          )));
              Fluttertoast.showToast(msg: 'Enquiry Updated');
            }).catchError((e) {
              print(e);
            });
          },
        ));
  }
}
