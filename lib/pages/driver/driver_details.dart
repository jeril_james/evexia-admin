import 'package:adminevexia/db/driver_detailUpload.dart';
import 'package:flutter/material.dart';

class DriverDetails extends StatefulWidget {
  final String userID;
  DriverDetails({
    this.userID
  });
  @override
  _DriverDetailsState createState() => _DriverDetailsState();
}

class _DriverDetailsState extends State<DriverDetails> {
  
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String password;
  String name;
  String department;
  String phone;
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.close,color: Colors.black),
            onPressed: (){    
              Navigator.of(context).pushNamed('/signup');
            },
          ),
        ),
      body: SingleChildScrollView(
      
              child: Column(
            children:[
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 30.0),
                child: Image(
                  image:AssetImage("images/verification.png"),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 12.0),
                child: Text(
                  "Please fill the details",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 11.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Poppins'
                  ),
                ),
              ),
              Container(
                // height: 180.0,
                margin: EdgeInsets.only(top:12.0,right:12.0,left:12.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: Column(
                  children:[
                    Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Container(
                                    height: 50.0,
                                    margin: const EdgeInsets.only(top: 12.0),
                                    padding: const EdgeInsets.only(
                                        left: 12.0, right: 12.0),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1.5, color: Color(0xdddddddd)),
                                      borderRadius: BorderRadius.circular(12.0),
                                      color: Colors.grey[200].withOpacity(0.6),
                                    ),
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return '\'Name Required\'';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        hintText: "Name",
                                        hintStyle:
                                            TextStyle(fontFamily: 'Poppins'),
                                        border: InputBorder.none,
                                      ),
                                      onChanged: (value) {
                                        this.name = value;
                                      },
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(top: 12.0),
                                    padding: const EdgeInsets.only(
                                        left: 12.0, right: 12.0),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1.5, color: Color(0xdddddddd)),
                                      borderRadius: BorderRadius.circular(12.0),
                                      color: Colors.grey[200].withOpacity(0.6),
                                    ),
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return '\'Name Required\'';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: "Phone Number",
                                        hintStyle:
                                            TextStyle(fontFamily: 'Poppins'),
                                      ),
                                      onChanged: (value) {
                                        this.phone = value;
                                      },
                                      keyboardType: TextInputType.phone,
                                     
                                    ),
                                  ),
                                  Container(
                                    height: 50.0,
                                    margin: const EdgeInsets.only(top: 12.0),
                                    padding: const EdgeInsets.only(
                                        left: 12.0, right: 12.0),
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1.5, color: Color(0xdddddddd)),
                                      borderRadius: BorderRadius.circular(12.0),
                                      color: Colors.grey[200].withOpacity(0.6),
                                    ),
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return '\'Name Required\'';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        hintText: "Password",
                                        hintStyle:
                                            TextStyle(fontFamily: 'Poppins'),
                                        border: InputBorder.none,
                                      ),
                                      onChanged: (value) {
                                        this.password = value;
                                      },
                                      obscureText: true,
                                    ),
                                  ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top:50.0,bottom: 20.0,right:30.0,left: 30.0 ),
                      height: 40,
                      child: RaisedButton(
                        color: Colors.indigo[300],
                        onPressed: (){
                          loading = true;
                          DriverDetailsUpload().updatePassword(
                            this.password,
                            this.name,
                            this.phone,
                            widget.userID,
                            context);
                          
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        child: Container(
                          child:  Center(
                            child:Text(
                              'Submit', 
                              style:  TextStyle(
                                fontSize: 18.0, 
                                color: Colors.white,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                              ),
                            ),  
                          ),
                        ),   
                      ),
                    ),      
                  ],
                ),
              ),
            ],
          ),
      ),
    );
  }
}