import 'package:adminevexia/components/doctor_list.dart';
import 'package:adminevexia/pages/driver/driver_card_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Drivers extends StatefulWidget {
  @override
  _DriversState createState() => _DriversState();
}

class _DriversState extends State<Drivers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
        actions: <Widget>[
          InkWell(
            onTap: (){
              Navigator.of(context).pushNamed('/all_doctor');
            },
                      child: Padding(
              padding: const EdgeInsets.only(right:18.0),
              child:Row(
                children: <Widget>[
                  Text('All Drivers'),
                  
                ],
              ),
            ),
          )
        ],
        backgroundColor: Colors.indigo[300],
        title: Text(
          'Drivers',
          style: TextStyle(
            fontSize:22.0,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child:Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                 Container(
                  height: 20,
                  padding: const EdgeInsets.only(top:0,left:12.0,right: 12.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(bottomLeft:Radius.circular(25.0),bottomRight: Radius.circular(25.0)),
                    color: Colors.indigo[300],
                  ),
                  ),
                // Padding(
                //   padding: const EdgeInsets.only(top: 0.0,left: 12.0,right: 12.0),
                //   child:SearchBarWidget(),
                // ),
              ],
            ),
            
            Container(
              decoration: BoxDecoration(            
               color: Colors.transparent,
              ),
              child: StreamBuilder(
                stream: Firestore.instance.collection('Users').where('usertype', isEqualTo: 'Driver').where('verification', isEqualTo: 'Pending').snapshots(),
                builder: (context,snapshot){
                  if(!snapshot.hasData){
                    return CircularProgressIndicator();
                  }
                  else
                  return ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (context,index){
                      return DriverCardWidget(
                        documentSnapshot: snapshot.data.documents[index],
                      );
                    },
                  );
                },
              )
            ),
          ],
        ),      
      ),
    );
  }
}