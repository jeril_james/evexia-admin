import 'package:adminevexia/db/profileCrud.dart';
import 'package:adminevexia/db/sharedPreference.dart';
import 'package:adminevexia/pages/homeSelect.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class DriverProfileEdit extends StatefulWidget {
  @override
  _DriverProfileEditState createState() => _DriverProfileEditState();
}

class _DriverProfileEditState extends State<DriverProfileEdit> {

  String id;
  bool _loading = true;
  final _formKey = GlobalKey<FormState>();
  String name;
  String department;
  String description;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getId();
  }

  void _getId() async{
    id = await SharedPrefer.getUid();
    setState(() {
      _loading = false;
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Firestore.instance.collection('/Users').document(id).snapshots(),
      builder: (context,snapshot){
        if(!snapshot.hasData){
          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
        else{
          var userDoc = snapshot.data;
          return Scaffold(
            appBar: AppBar(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(16.0),
                    bottomRight: Radius.circular(16.0)),
              ),
              elevation: 0,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              backgroundColor: Colors.indigo[300],
              title: Text(
                'Please fill your details ..',
                style: TextStyle(
                  fontSize: 18.0,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
            body: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                        top: 12.0, right: 12.0, left: 12.0, bottom: 12.0),
                    margin:
                        EdgeInsets.only(right: 12.0, left: 12.0, bottom: 12.0),
                    width: double.maxFinite,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.15,
                          width: MediaQuery.of(context).size.width,
                          child: Stack(
                            children: <Widget>[
                              Positioned.fill(
                                child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.of(context)
                                          .pushNamed('/driver_image');
                                    },
                                    child: Container(
                                      height: 110.0,
                                      width: 110.0,
                                      child: CircleAvatar(
                                        radius: 30.0,
                                        backgroundImage: NetworkImage(
                                            userDoc['image'] ??
                                                'https://firebasestorage.googleapis.com/v0/b/evexia-e1062.appspot.com/o/profile-pic%2Fasset-1.png?alt=media&token=ae13e840-c6e8-404e-b16a-60a50c52497e'),
                                        backgroundColor: Colors.transparent,
                                      ),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                          color: Colors.indigo[300],
                                          width: 2.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Positioned.fill(
                                child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 100.0, top: 10.0),
                                    child: Container(
                                      height: 40.0,
                                      width: 40.0,
                                      child: Icon(
                                        Icons.photo_camera,
                                        color: Colors.indigo[300],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        Form(
                          key: _formKey,
                          child: Container(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  height: 50.0,
                                  margin: const EdgeInsets.only(top: 12.0),
                                  padding: const EdgeInsets.only(
                                      left: 12.0, right: 12.0),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 1.5, color: Color(0xdddddddd)),
                                    borderRadius: BorderRadius.circular(12.0),
                                    color: Colors.grey[200].withOpacity(0.6),
                                  ),
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return '\'Name Required\'';
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      hintText: "Name",
                                      hintStyle:
                                          TextStyle(fontFamily: 'Poppins'),
                                      border: InputBorder.none,
                                    ),
                                    onChanged: (value) {
                                      this.name = value;
                                    },
                                  ),
                                ),
                                Row(
                                    children: <Widget>[
                                      
                                      
                                      // Container(
                                      //   height: 50.0,
                                      //   width: MediaQuery.of(context).size.width * 0.8,
                                      //   margin: const EdgeInsets.only(top: 12.0),
                                      //   padding: const EdgeInsets.only(
                                      //       left: 12.0, right: 12.0),
                                      //   decoration: BoxDecoration(
                                      //     border: Border.all(
                                      //         width: 1.5,
                                      //         color: Color(0xdddddddd)),
                                      //     borderRadius:
                                      //         BorderRadius.circular(12.0),
                                      //     color:
                                      //         Colors.grey[200].withOpacity(0.6),
                                      //   ),
                                      //   child: DropdownButtonFormField(
                                      //     value: department,
                                      //     hint: Text('Department'),
                                      //     items: <String>[
                                      //       'General Medicine',
                                      //       'Coronavirus treatment',
                                      //       'Geriatric',
                                      //       'Lab',
                                      //       'Blood Bank',
                                      //       'First Aid',
                                      //       'Paediatric',
                                      //       'Urology',
                                      //     ].map<DropdownMenuItem<String>>(
                                      //         (String value) {
                                      //       return DropdownMenuItem<String>(
                                      //         value: value,
                                      //         child: Text(value),
                                      //       );
                                      //     }).toList(),
                                      //     onChanged: (value) {
                                      //       setState(() {
                                      //         this.department = value;
                                      //       });
                                      //     },
                                      //   ),
                                      // ),
                                    ],
                                  ),
                                Container(
                                  height: 50.0,
                                  margin: const EdgeInsets.only(top: 12.0),
                                  padding: const EdgeInsets.only(
                                      left: 12.0, right: 12.0),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 1.5, color: Color(0xdddddddd)),
                                    borderRadius: BorderRadius.circular(12.0),
                                    color: Colors.grey[200].withOpacity(0.6),
                                  ),
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return '\'Description\'';
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: "Description",
                                      hintStyle:
                                          TextStyle(fontFamily: 'Poppins'),
                                    ),
                                    onChanged: (value) {
                                      this.description = value;
                                    },
                                    keyboardType: TextInputType.text,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0,
              color: Colors.transparent,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 18.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                      elevation: 0,
                      // materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          Navigator.of(context).pop();
                          ProfileCrud().addData({
                            'name': this.name,
                            'department':this.department,
                            'description':this.description,
                          },
                          id).then((result) {
                           Navigator.push(context, MaterialPageRoute(builder: (context)=>HomeSelect(userType: id,)));
                          }).catchError((e) {
                            print(e);
                          });
                        }
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0)),
                      color: Colors.indigo[300],
                      child: Container(
                        margin: EdgeInsets.only(
                            left: 45.0, right: 45.0, top: 12, bottom: 12),
                        child: Text(
                          'Update Details',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 12.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        }
      },
    );
  }
}