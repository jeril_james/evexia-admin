import 'package:adminevexia/db/sharedPreference.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class DoctorChat extends StatefulWidget {
  final String enquiryID;
  DoctorChat({
    this.enquiryID
  });


  @override
  _DoctorChatState createState() => _DoctorChatState();
}

class _DoctorChatState extends State<DoctorChat> {

  String id;
  bool loading = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getId();
  }

  void _getId() async{
    id = await SharedPrefer.getUid();
    setState(() {
      loading = false;
    });
  }
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _firestore = Firestore.instance;

  TextEditingController messageController = TextEditingController();
  ScrollController scrollController = ScrollController();


  Future<void> callback()async{
    if(messageController.text.length > 0){
      await _firestore.collection('messages').document(widget.enquiryID).collection('chat').add({
        'text':messageController.text,
        'from':id,
        'date':DateTime.now().toIso8601String().toString()
      });
      messageController.clear();
      scrollController.animateTo(
        scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        backgroundColor: Colors.indigo[300],
        title: Text(
          'Chat',
          style: TextStyle(
            fontSize: 18.0,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ),
      body: Container(
        color: Colors.black12,
        child: SafeArea(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
                child: StreamBuilder<QuerySnapshot>(
                    stream: _firestore.collection('messages')
                    .document(widget.enquiryID)
                    .collection('chat').orderBy('date')
                    .snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) 
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                       
                      List<DocumentSnapshot> docs = snapshot.data.documents;
                      List<Widget> messages =  docs.map((doc)=> Message(
                        from: doc.data['from'],
                        text: doc.data['text'],
                        me: id == doc.data['from'],
                      )).toList();
                      
                        return ListView(
                          controller: scrollController,
                          children: <Widget>[
                            ...messages,
                          ],
                        );
                    })),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          onSubmitted: (value)=>callback(),
                          controller: messageController,
                          decoration: InputDecoration(
                            hintText: 'Enter a Message',
                            border: const OutlineInputBorder()
                          ),
                        )
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SendButton(
                          icon: Icon(Icons.forward),
                          callback: callback,
                        ),
                      )
                    ],
            )),
              )
          ],
        )),
      ),
    );
  }
}
class SendButton extends StatelessWidget {

  final Icon icon;
  final VoidCallback callback;
  const SendButton({
    Key key,
    this.icon,
    this.callback
  }): super(key:key);

  @override
  Widget build(BuildContext context) {
    
    return FloatingActionButton(
      backgroundColor: Colors.indigo[300],
      onPressed: callback,
      child: icon
    );
  }
  
}

class Message extends StatelessWidget {

  final String from;
  final String text;
  final bool me;

  const Message({Key key, this.from, this.text, this.me}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: me ?CrossAxisAlignment.end :CrossAxisAlignment.start,
        children: <Widget>[
          // Text(from),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Material(
              color: me? Colors.blueAccent: Colors.indigo[300],
              borderRadius: BorderRadius.circular(10.0),
              elevation: 6.0,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10.0,horizontal: 15.0),
                child: Text(text),
              ),
            ),
          )
        ],
      ),
    );
  }
}