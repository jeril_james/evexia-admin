import 'package:adminevexia/db/sharedPreference.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class DoctorUserPage extends StatefulWidget {
  @override
  _DoctorUserPageState createState() => _DoctorUserPageState();
}

class _DoctorUserPageState extends State<DoctorUserPage> {
  bool _loading = true;
  String id;

  @override
  void initState() {
    super.initState();
    _getId();
    
  }

  void _getId() async {
    id = await SharedPrefer.getUid();
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream:
            Firestore.instance.collection('/Users').document(id).snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            var userDoc = snapshot.data;
            return Scaffold(
              appBar: AppBar(
                // shape: RoundedRectangleBorder(
                //   borderRadius: BorderRadius.only(
                //       bottomLeft: Radius.circular(16.0),
                //       bottomRight: Radius.circular(16.0)),
                // ),
                elevation: 0,
                backgroundColor: Colors.indigo[300],
                title: Text(
                  'My Account',
                  style: TextStyle(
                    fontSize: 18.0,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
              body: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Container(
                          height: 150,
                          padding: const EdgeInsets.only(
                              top: 40, left: 12.0, right: 12.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(25.0),
                                bottomRight: Radius.circular(25.0)),
                            color: Colors.indigo[300],
                          ),
                        ),
                        Stack(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(26.0),
                              margin: EdgeInsets.only(
                                  top: 33.0, left: 14.0, right: 14.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.4),
                                      offset: Offset(2, 4),
                                      blurRadius: 10)
                                ],
                                color: Colors.white,
                              ),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          IconButton(
                                              icon: Icon(
                                                Icons.edit,
                                                color: Colors.indigo[300],
                                              ),
                                              onPressed: () {
                                                Navigator.of(context).pushNamed(
                                                    '/doctor_profile_edit');
                                              })
                                        ],
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        userDoc.data['name'] ?? 'aa',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Center(
                                        child: Text(
                                          userDoc['email'] ?? '',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 14.0,
                                            color: Colors.grey,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Text(
                                        'Department :  ',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 14.0,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Text(
                                        userDoc['department'] ?? '',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 14.0,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          userDoc['description'] ?? '',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 14.0,
                                            color: Colors.black,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Center(
                                child: ball(
                              userDoc.data['image']??'',
                              Colors.white,
                            )),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    // Text(
                    //   'Latest Enquiries',
                    //   style: TextStyle(
                    //       color: Colors.indigo[300],
                    //       fontWeight: FontWeight.bold),
                    // ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  Widget ball(
    String image,
    Color color,
  ) {
    return Container(
      height: 70,
      width: 70.0,
      decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(100.0),
          border: Border.all(width: 1, color: Colors.grey.withOpacity(0.2)),
          image: DecorationImage(
            image: NetworkImage(image ??
                'https://firebasestorage.googleapis.com/v0/b/evexia-e1062.appspot.com/o/profile-pic%2Fasset-1.png?alt=media&token=ae13e840-c6e8-404e-b16a-60a50c52497e'),
            fit: BoxFit.cover,
          )),
    );
  }

  Widget card(String image, String title, String subTitle, String rank) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          ball(image, Colors.transparent),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                title,
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0),
              ),
              Text(
                subTitle,
                textAlign: TextAlign.left,
                style: TextStyle(fontFamily: 'Poppins', fontSize: 10.0),
              ),
              Text(
                "50 \$",
                textAlign: TextAlign.left,
                style: TextStyle(fontFamily: 'Poppins', fontSize: 10.0),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.star,
                color: Colors.yellow,
              ),
              Text(
                rank,
                style: TextStyle(
                  fontFamily: 'Poppins',
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
