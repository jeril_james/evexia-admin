import 'package:adminevexia/pages/doctor/doctorFull_card.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AllDoctor extends StatefulWidget {
  @override
  _AllDoctorState createState() => _AllDoctorState();
}

class _AllDoctorState extends State<AllDoctor> {

  final _formKey = GlobalKey<FormState>();
  String name;
  String department;
  String description;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(16.0),
                    bottomRight: Radius.circular(16.0)),
              ),
              elevation: 0,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              backgroundColor: Colors.indigo[300],
              title: Text(
                'All Doctors',
                style: TextStyle(
                  fontSize: 18.0,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
      body: SingleChildScrollView(
              child: StreamBuilder(
                stream: Firestore.instance.collection('/Users').where('usertype', isEqualTo: 'Doctor').snapshots(),
                builder: (context,snapshot){
                  if(!snapshot.hasData){
                    return CircularProgressIndicator();
                  }
                  else
                  return ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (context,index){
                      return DoctorCardFull(
                        documentSnapshot: snapshot.data.documents[index],
                      );
                    },
                  );
                  })
            ),
            
    );
  }
  Widget ball(String image, Color color) {
    return Container(
      height: 80,
      width: 80.0,
      decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(100.0),
          image: DecorationImage(
            image: AssetImage(image),
            fit: BoxFit.cover,
          )),
    );
  }
}