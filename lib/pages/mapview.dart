import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapView extends StatefulWidget {
  final double lati,lang;
  MapView({
    this.lati,
    this.lang
  });
  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo[100],
       
      ),
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: 
            Stack(
              children: <Widget>[
                GoogleMap(
                  mapType: MapType.normal,
                  myLocationEnabled: true,
                  myLocationButtonEnabled: true,
                  compassEnabled: true,
                  initialCameraPosition: CameraPosition(
                    target: LatLng(widget.lati,widget.lang),
                    zoom: 13.0
                  ),
                  scrollGesturesEnabled: true,
                  buildingsEnabled: true,
                  markers: Set<Marker>.of(
                    <Marker>[
                      Marker(
                        draggable: true,
                        markerId: MarkerId('1'),
                        position: LatLng(widget.lati,widget.lang),
                        
                      )
                    ]
                  ),
                  
                  
            ),
            // Positioned(
            //   top: MediaQuery.of(context).size.height * 0.07,
            //   right: 15.0,
            //   left: 15.0,
            //   child: Container(
            //     height: 50,
            //     width: double.infinity,
            //     decoration: BoxDecoration(
            //       borderRadius: BorderRadius.circular(10.0),
            //       color: Colors.white
            //     ),
                
            //   ),
            // ),
              ],
            )
          ),
        ],
      ),
    );
  }
}